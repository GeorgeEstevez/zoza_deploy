// Filter table

$(document).ready(function(){
  $("#barsearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#Tabs tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});